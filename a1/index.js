const http = require("http");

const port = 3000

const server = http.createServer((request,response) => {

	if(request.url == '/login'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to the login page.`)
	}
		
	else{

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 - This Page Not Found!`)
	}

})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);
